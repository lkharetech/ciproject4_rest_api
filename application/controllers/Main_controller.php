<?php
require(APPPATH . 'libraries/REST_Controller.php');

class Main_controller extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    // Insert Code 
    function index_post()
    {
        $id = $this->input->post('id');
        
        $data = array(
            'name' => $this->input->post('name'),
            'city' => $this->input->post('city')
        );

        if($id != '') {
            $query = $this->db->update('users', $data, array('id'=>$id));
        } else {
            $query = $this->db->insert('users', $data);
        }

        if($query == 1) {
            $this->response(['Data inserted/updated successfully.'], REST_Controller::HTTP_OK);
        } else {
            echo "Something went wrong while inserting data !!";
        }
    }
    // Listing & Get Data code
    function index_get($id = 0) {

        $query = $this->db->get('users')->result();

        $response["data"] = $query;
        if($response != "") {
            echo json_encode($response);
        } else {
            echo "SOMETHING WENT WRONG WHILE LISTING DATA";
        }
        
    }
    // Delete Code 
    function index_delete() {
        $id = $this->delete('id');

        $result = $this->db->delete('users', array('id'=>$id));
       
        if($result == true) {
            $this->response(['Data deleted successfully.'], REST_Controller::HTTP_OK);
        } else {
            echo "Data not deleted !!";
        }        
    }
    // Single data get code
    function single_get() {
        $id = $this->input->get('id');
        
        $data = $this->db->get_where("users", ['id' => $id])->row_array();

        if ($id) {
			$this->response(array('message' => 'successfully','data'=> $data), REST_Controller::HTTP_OK);
		} else {
			$this->response(array('message' => 'failed'), REST_Controller::HTTP_BAD_REQUEST);
		}
    }
}
