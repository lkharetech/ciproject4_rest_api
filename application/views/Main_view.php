<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>

    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>

    <title>Document</title>
</head>

<body>
    <div class="container">
        <hr>
        <h2 style="text-align: center;">REST API SIMPLE CRUD CI 3</h2>
        <hr>
        <div>
            <form method="" action="" id="user_form">
                <label for="">Name : </label>
                <input type="text" name="name" id="name" placeholder="Enter your name"><br><br>
                <label for="">City : </label>
                <input type="text" name="city" id="city" placeholder="Enter your city"><br><br>
                <input type="hidden" name="id" id="id">
                <input type="submit" name="submit" id="submit" value="ADD">
            </form>
        </div>
        <hr>
        <table>
            <thead>
                <tr>
                    <th width="10%">#</th>
                    <th width="20%">NAME</th>
                    <th width="20%">CITY</th>
                    <th width="50%">ACTION</th>
                </tr>
            </thead>
            <tbody id="tablebody">

            </tbody>
        </table>
    </div>

</body>

</html>

<script>
    listingData();
    // Validate form during insert & add
    $("#user_form").validate({
        rules: {
            name: {
                required: true,
            },
            city: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "Name can't be blank !!",
            },
            city: {
                required: "City can't be blank !!",
            }
        },
        // Add data function
        submitHandler: function(form) {
            $.ajax({
                url: "main_controller",
                type: "POST",
                dataType: "JSON",
                processData: false,
                contentType: false,
                data: new FormData($('#user_form')[0]),
                success: function(response) {
                    console.log(response);
                    document.getElementById("user_form").reset();
                    listingData();
                }
            });
        }
    });
    // Listing the details
    function listingData() {
        $.ajax({
            url: "main_controller",
            type: "GET",
            async: false,
            dataType: "JSON",
            success: function(response) {
                console.log("Listing Successful !!");

                var html = '';
                var count = 0;
                for (var i = 0; i < response.data.length; i++) {
                    html += '<tr>';
                    html += '<td>' + ++count; + '</td>';
                    html += '<td>' + response.data[i].name + '</td>';
                    html += '<td>' + response.data[i].city + '</td>';
                    html += '<td>';
                    html += '<button onclick="editData(' + response.data[i].id + ')">EDIT</button>';
                    html += '<button onclick="deleteData(' + response.data[i].id + ')">DELETE</button>';
                    html += '</td>';
                    html += '</tr>';
                }
                $("#tablebody").html(html);
                $("#submit").val("ADD");
                $("#id").val(null);
                document.getElementById("user_form").reset();
            }
        });
    }
    // Delete data function
    function deleteData(id) {
        $.ajax({
            url: "main_controller",
            type: 'DELETE',
            dataType: 'JSON',
            data: {
                id: id
            },
            success: function(response) {
                console.log(response);
                listingData();
            }
        });
    }
    // Edit data function 
    function editData(id) {
        $.ajax({
            url: "main_controller/single",
            type: 'GET',
            dataType: 'JSON',
            data: {
                id: id
            },
            success: function(response) {
                console.log("SINGLE DATA SUCCESSFULLY GET !!");
                $("#name").val(response.data.name);
                $("#city").val(response.data.city);
                $("#id").val(response.data.id);
                $("#submit").val('UPDATE');
            }
        });
    }
</script>